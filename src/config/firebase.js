import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyASUe3T1HTPlfXxe0t-ilirH_LxT0JRgXU",
    authDomain: "chousei-firebase-41664.firebaseapp.com",
    databaseURL: "https://chousei-firebase-41664-default-rtdb.firebaseio.com",
    projectId: "chousei-firebase-41664",
    storageBucket: "chousei-firebase-41664.appspot.com",
    messagingSenderId: "310562846180",
    appId: "1:310562846180:web:5f20cde04455d2f32903f1"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
